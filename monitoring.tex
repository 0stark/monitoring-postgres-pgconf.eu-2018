\documentclass[9pt,aspectratio=43,t]{beamer}
%\documentclass[10pt,aspectratio=169,t]{beamer}
\setbeamersize{text margin left=5mm,text margin right=5mm} 
\usepackage{hyperref}
\usepackage{lmodern}
\usepackage{minted}
\setminted{fontsize=\small}
\usepackage[latin1]{inputenc}
\usepackage{tikz}
\usetikzlibrary{arrows.meta,positioning,matrix,shapes}
\usetheme{Warsaw}
\title[Monitoring Postgres]{Using Prometheus and Grafana to build a Postgres Dashboard}
\author{Gregory Stark}
\date{October 25, 2018}

\setbeamertemplate{navigation symbols}{}

\makeatletter
\newcommand<>{\fullsizegraphic}[1]{
    \global\beamer@shrinktrue%
    \gdef\beamer@shrinkframebox{%
      \setbox\beamer@framebox=\vbox to\beamer@frametextheight{%
\hspace*{-5mm}\includegraphics[keepaspectratio,height=\beamer@frametextheight,width=\paperwidth]{#1}
      }
    }
}
\makeatother

\tikzset{%
  >={Latex[width=2mm,length=2mm]},
  % Specifications for style of nodes:
  base/.style = {rectangle, rounded corners, draw=black,
    minimum width=1cm, minimum height=1cm,
    text centered, font=\sffamily},
  pg/.style = {base, fill=blue!10},
  data/.style = {base, fill=red!10},
  agent/.style = {base, fill=green!10},
  gui/.style = {base, fill=orange!10},
}

\begin{document}



\begin{frame}
  \titlepage
\end{frame}


\begin{frame}[shrink]{What is Monitoring?}
  \begin{figure}
    \includegraphics[height=4in]{wordcloud.png}
  \end{figure}
\end{frame}


\begin{frame}{Old School}
  \begin{tikzpicture}[node distance=5mm and 5mm, align=center]
    \node (server) [pg] {Server};
    \node (nagios) [below=of server] {Nagios Monitor}
    edge [<-] (server);
  \end{tikzpicture}
\end{frame}

\begin{frame}{New School}
  \begin{itemize}
  \item We want to alert on global properties such as
    \begin{itemize}
      \item The fraction of the fleet currently operating well
      \item The average response time across the fleet
      \item The consistency of the data across the fleet
    \end{itemize}
  \item We want to alert based on historical data
    \begin{itemize}
    \item Average rates over time period
    \item Compare current data with 24h ago or 7d ago
    \end{itemize}
  \item We want to alert on comparisons between services
    \begin{itemize}
    \item Ratio of rates of transactions in database to application requests
    \item Are there any database servers for which S3 does not contain a recent backup
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[shrink]{The Tools}
  \begin{itemize}
  \item Prometheus
    \begin{quote}
    Database specifically designed for handling time series. It
    performs recorded queries regularly to synthesize new time series
    and to generate alerts.
    \end{quote}
  \item Alertmanager
    \begin{quote}
    Part of Prometheus project. Handles generating notifications for
    alerts.
    \end{quote}
  \item node\_exporter
    \begin{quote}
    Agent for system statsitics. For more agents see:
    https://prometheus.io/docs/instrumenting/exporters/
    \end{quote}
  \item postgres\_exporter
    \begin{quote}
    Agent that exports statistics from {\tt pg\_stat\_*} views
    \end{quote}
  \item mtail
    \begin{quote}
    Useful to fill gaps where Postgres doesn't provide a statistics
    views to expose them. e.g.  {\tt log\_min\_duration}, {\tt
      log\_lock\_waits}
    \end{quote}
  \item Grafana
    \begin{quote}
    WYSIWYG dashboard software. 
    \end{quote}
  \end{itemize}
\end{frame}

\begin{frame}{The Data Flow}
  \begin{centering}
    \begin{tikzpicture}[node distance=5mm and 5mm, align=center]
      \node (pg) at (100,0) [pg] {PostgreSQL};
      \node<1> (pgstats1) [data, below=of pg, align=left] {%
        \begin{tabular}{ll}
          pg\_stat\_activity           &  pg\_stat\_archiver           \\
          pg\_stat\_replication        &  pg\_stat\_bgwriter           \\
          pg\_stat\_wal\_receiver      &  pg\_stat\_database           \\
          pg\_stat\_subscription       &  pg\_stat\_database\_conflicts\\
          pg\_stat\_ssl                & \\
          pg\_stat\_progress\_vacuum   & \\\\
          pg\_stat\_all\_tables        &  pg\_statio\_all\_tables      \\
          pg\_stat\_all\_indexes       &  pg\_statio\_all\_indexes     \\
          pg\_stat\_user\_functions    &  pg\_statio\_all\_sequences
        \end{tabular}
      } edge [<-] (pg);
      \node<2-> (pgstats)      [data, below=of pg]   {PG Statistics Views\\pg\_stat\_*} edge [<-] (pg);
      \node<3-> (logs)     [data, left=of pgstats]          {Logs} edge [<-] (pg);
      \node<4-> (pgstatsstatements)     [data, right=of pgstats]   {Other Tables\\pg\_stat\_statements} edge [<-] (pg);

      \node<5-> (postgresexporter) [agent, below=of pgstats] {postgres\_exporter} edge [<-] (pgstats);
      \path<5-> (pgstatsstatements) edge [->,bend left] node [below,sloped] {queries.yaml} (postgresexporter);
      \node<6-> (mtail) [agent, below=of logs] {mtail} edge [<-] (logs);

      \node<7-> (prometheus) [pg, below=of postgresexporter] {Prometheus}
      edge [<-] (postgresexporter)
      edge [<-] (mtail)
      ;
      \node<8-> (os) [pg, right=of pgstatsstatements, yshift=1cm] {Operating\\System};
      \node<8-> (nodeexporter) [agent, below=of os] {node\_exporter}
      edge [<-] (os)
      edge [->, bend left=15] (prometheus);
      \node<8-> (app) [pg, right=of os] {Application\\Metrics}
      edge [->, bend left=40] (prometheus.east);

      \node<9-> (grafana) [gui, below=of prometheus] {Grafana}
      edge [<-] (prometheus);
      \node<9-> (alerts) [gui, right=of grafana] {%
        \begin{tabular}{ll}
          Email & IRC \\
          Pagerduty & Slack
        \end{tabular}
      }
      edge [<-] (prometheus);

    \end{tikzpicture}
  \end{centering}
\end{frame}



\begin{frame}{USE}
The USE method uses three key metrics for each component of a complex system:
  \begin{itemize}
  \item Utilization
  \item Saturation
  \item Errors
  \end{itemize}

It was published in ACMQ as Thinking Methodically about Performance (2012):\\
https://queue.acm.org/detail.cfm?id=2413037
\\

Further discussion:\\
http://www.brendangregg.com/usemethod.html
\\

Presented at FISL13:\\
http://dtrace.org/blogs/brendan/2012/09/21/fisl13-the-use-method/
\end{frame}

\begin{frame}{RED}

  The RED model uses latency (duration)
  instead of utilization:

  \begin{itemize}
  \item Rate
  \item Errors
  \item Duration
  \end{itemize}

  From:\\
  https://www.weave.works/blog/the-red-method-key-metrics-for-microservices-architecture/
\\

  See also:\\
  https://www.vividcortex.com/blog/monitoring-and-observability-with-use-and-red
\end{frame}

\begin{frame}{Google's SRE Golden Signals}
SRE Golden Signals are very similar:
  \begin{itemize}
  \item Latency
  \item Traffic
  \item Errors
  \item Saturation
  \end{itemize}

Orginally published in Site Reliability Book:\\
    \includegraphics[height=1in]{sre-book.jpg}
    \\
    
Also see discussion at:\\
https://medium.com/devopslinks/how-to-monitor-the-sre-golden-signals-1391cadc7524
\end{frame}

\begin{frame}{PromQL}
\fullsizegraphic{prometheus-1.png}
\end{frame}
\begin{frame}{PromQL}
\fullsizegraphic{prometheus-2.png}
\end{frame}
\begin{frame}{PromQL}
\fullsizegraphic{prometheus-3.png}
\end{frame}

% Rate
\begin{frame}{Alerts - Rate}
% pg_stat_database_xact_commit
\inputminted{yaml}{rules-3.yaml}
\end{frame}

% Errors
\begin{frame}{Alerts - Errors}
% pg_stat_database_rollback
\inputminted{yaml}{rules-5.yaml}
\end{frame}

% Saturation
\begin{frame}{Alerts - Saturation}
% pg_stat_activity_count
\inputminted{yaml}{rules-4.yaml}
\end{frame}

% More errors using mtail
\begin{frame}{Alerts - more Errors}
\inputminted[fontsize=\scriptsize]{perl}{mtail-1.mtail}
\end{frame}
\begin{frame}{Alerts}
% postgresql_errors_total
\inputminted{yaml}{rules-6.yaml}
\end{frame}

% Replication lag
\begin{frame}{Alerts - Exposing hidden problems}
\inputminted{yaml}{queries-1.yaml}
\end{frame}
\begin{frame}{Alerts - Exposing hidden problems}
% pg_replication_lag
\inputminted{yaml}{rules-7.yaml}
\end{frame}

% Replication slots danger
\begin{frame}{Alerts - Exposing hidden problems}
\inputminted[fontsize=\scriptsize]{yaml}{queries-2.yaml}
\end{frame}
\begin{frame}{Alerts - Exposing hidden problems}
% pg_replication_slots_active
\inputminted{yaml}{rules-9.yaml}
\end{frame}

%\begin{frame}{Alerts - Exposing hidden problems}
%  XXX Split brain is a critical problem
%  This is an example of the advantage of the ``global view'' of the environment
%\end{frame}
\begin{frame}{Alerts - Exposing hidden problems}
% split brain!
\inputminted{yaml}{rules-10.yaml}
\end{frame}

\begin{frame}{Alerts - Miscellaneous}
% fleet change, settings change warnings
\inputminted{yaml}{rules-11.yaml}
\end{frame}

\begin{frame}{The GUI Dashboard}
\fullsizegraphic{dashboard-1.png}
\end{frame}
\begin{frame}{The GUI Dashboard}
\fullsizegraphic{dashboard-2.png}
\end{frame}
\begin{frame}{The GUI Dashboard}
\fullsizegraphic{dashboard-3.png}
\end{frame}

\begin{frame}{pg\_stat\_statements}
\inputminted[fontsize=\scriptsize]{yaml}{queries-pgss-1.yaml}
\end{frame}
\begin{frame}{pg\_stat\_statements}
\inputminted[fontsize=\scriptsize]{yaml}{queries-pgss-2.yaml}
\end{frame}
\begin{frame}{pg\_stat\_statements}
  This has some issues with Cardinality....
  \begin{itemize}
  \item 15 metrics
  \item for each of 5000 queryids (or more)
  \item for each database
  \item every 15s
  \end{itemize}
  This can quickly become performance issue for Prometheus.
\end{frame}
\begin{frame}{pg\_stat\_statements}
\fullsizegraphic{dashboard-pgss-1a.png}
\end{frame}
\begin{frame}{pg\_stat\_statements}
\fullsizegraphic{dashboard-pgss-1b.png}
\end{frame}
%\begin{frame}{pg\_stat\_statements}
%\fullsizegraphic{dashboard-pgss-3.png}
%\end{frame}
\begin{frame}{pg\_stat\_statements}
\fullsizegraphic{dashboard-pgss-4.png}
\end{frame}
\begin{frame}{pg\_stat\_statements}
\fullsizegraphic{dashboard-pgss-5.png}
\end{frame}
\begin{frame}{pg\_stat\_statements}
\fullsizegraphic{dashboard-pgss-6.png}
\end{frame}

% \begin{frame}{General Flow}
% \tikzset{%
%   >={Latex[width=2mm,length=2mm]},
%   % Specifications for style of nodes:
%   base/.style = {rectangle, draw=black,
%     minimum width=.1cm, minimum height=.1cm,
%     text centered, font=\sffamily},
%   box/.style = {base, rectangle, fill=blue!10},
%   branch/.style = {base, diamond, aspect=2, fill=red!10, inner sep=0pt},
% }
% 
%     \begin{tikzpicture}[node distance=30mm and 20mm, align=center]
%       \node (start) at (100,0) [box] {Need a new metric};
%       \node (where) [branch, below of=start] {Identify where Postgres exposes it};
%       \node (exporter) [box, below of=where] {Add postgres\_exporter queries.yaml query};
%       \node (mtail) [box, left of=exporter]  {Add regexp and counter to mtail};
%       \node (neither) [box, right of=exporter] {Export from application or other exporter};
%       \node (prometheus) [box, below of=exporter] {Develop expressions in Prometheus Dashboard};
%       \node (alerts) [box, below of=prometheus] {Write alert using expression};
%       \node (dashboard) [box, right of=alerts] {Add graph in Grafana using expression};
%       \node (rules) [box, right of=dashboard] {Write recorded rules for aggregates needed};
%       \node (oops) [branch, below of=alerts] {Test metric in production};
%       \draw [->] (start) -- (where);
%       \draw [->] (where.west) -- [xshift=1cm] -- (mtail);
%       \draw [->] (where.south) -- (exporter);
%       \draw [->] (where.east) -- (neither);
%       \draw [->] (mtail) -- (prometheus.north);
%       \draw [->] (exporter) -- (prometheus);
%       \draw [->] (neither) -- (prometheus);
%       \draw [->] (prometheus) -- (alerts);
%       \draw [->] (prometheus) -- (dashboard);
%       \draw [->] (prometheus) -- (rules);
%       \draw [->] (alerts) -- (oops);
%       \draw [->] (dashboard) -- (oops);
%       \draw [->] (rules) -- (oops);
%       \draw [->] (oops.west) -- (start);
%     \end{tikzpicture}
% \end{frame}

\begin{frame}{Future things to address}
  \begin{itemize}
  \setlength{\itemsep}{18pt}

  \item Missing stats in postgres\_exporter

    Queries.yaml requires a good understanding of Postgres *and*
    Prometheus to write. It also makes rules and dashboards
    non-portable which is a major downside. It's considered an
    ``anti-pattern'' in Prometheus exporter world.

  \item Missing data in {\tt pg\_stats\_*}: errors, lock timing, slow queries

    If you have more please tell me, I'll be working on adding these
    in the future.

  \item Saturation is basically impossible to measure in Postgres

    {\tt pg\_stat\_activity} does not really represent saturation very
    well when applications keep persistent connections and use pooling
    of any form. If you filter on {\tt state=active} it's useful but
    still very coarse and incomplete representation.

    There's a pgbouncer exporter as well and you can add
    instrumentation to your application to address this. But it would
    be good to identify standard ways of measuring Postgres
    saturation.

  \end{itemize}
\end{frame}

\begin{frame}{Future things to address}
  \begin{itemize}
  \setlength{\itemsep}{18pt}

  \item postgres\_exporter should be eliminated entirely

    It would be much preferable to have Postgres speak common
    monitoring protocols directly. That would make the statistics more
    consistent, reliable, and easier to deploy.

  \item Distributed Tracing

    This is different from but complementary to monitoring and is a
    major gap that would help expose the connections between database
    metrics and application metrics.

  \end{itemize}
\end{frame}

\begin{frame}{More information}
  \begin{itemize}
  \setlength{\itemsep}{18pt}
  \item This presentation is online at:\\
    {\small\url{https://0stark.gitlab.io/monitoring-postgres-pgconf.eu-2018/monitoring.pdf}}

  \item Gitlab Project for presentation at:\\
    \url{https://gitlab.com/0stark/monitoring-postgres-pgconf.eu-2018}

  \item Source code for presentation at:\\
    \url{https://gitlab.com/0stark/monitoring-postgres-pgconf.eu-2018.git}

  \item Author contact address:\\
    \url{stark@mit.edu}

  \end{itemize}
\end{frame}



% \begin{frame}{Alerts}
% % pg_txid_current (queries.yaml)
% \inputminted{yaml}{rules-1.yaml}
% \end{frame}

% \begin{frame}{Alerts}
% % pg_xlog_position_bytes (queries.yaml)
% \inputminted{yaml}{rules-2.yaml}
% \end{frame}

% \begin{frame}{Alerts}
% % pg_xlog_postion_bytes - consumption high
% \inputminted{yaml}{rules-8.yaml}
% \end{frame}alecthomas/chroma

\end{document}
