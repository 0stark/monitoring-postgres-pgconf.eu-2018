#!/usr/bin/python

import json
import sys
import re

def fixpanel(p):
    if 'datasource' in  p:
        if p['datasource'] != 'Prometheus in Azure':
            print p['datasource']
        p['datasource'] = '${DS_PROMETHEUS}'
        for t in p['targets']:
            if 'dsType' in t:
                del t['dsType']
            if 'expr' in t:
                expr = t['expr']
                expr = re.sub('(?i)fqdn|instance|host', 'instance', expr)
                expr = re.sub('(?i)datname="[^"]*"', '', expr)
                expr = re.sub('(?i)tier="[^"]*"', '', expr)
                expr = re.sub('(?i)type="[^"]*"', '', expr)
                t['expr'] = expr
            if 'legendFormat' in t:
                legendFormat = t['legendFormat']
                t['legendFormat'] = re.sub('(?i){{(fqdn|instance|host)}}', '{{instance}}', legendFormat)
    if 'repeat' in p and p['repeat']:
        repeat = p['repeat']
        p['repeat'] = re.sub('(?i)(fqdn|instance|host)', 'instance', repeat)
    if 'scopedVars' in p:
        for varname in p['scopedVars'].keys():
            if varname.lower() in ('fqdn','host'):
                p['scopedVars']['instance'] = p['scopedVars'][varname]
                del p['scopedVars'][varname]
    if 'title' in p:
        title = p['title']
        p['title'] = re.sub('(?i)\[\[(fqdn|instance|host)\]\]', '[[instance]]', title)
    if 'styles' in p:
        for s in p['styles']:
            if 'linkUrl' in s:
                linkurl = s['linkUrl']
                retval = re.sub('(?i)\[\[(fqdn|instance|host)\]\]', '[[instance]]', linkurl)
                retval = re.sub('(?i)var-(fqdn|instance|host)', 'var-instance', linkurl)
                s['linkUrl'] = retval
            if 'pattern' in s:
                pattern = s['pattern']
                s['pattern'] = re.sub('(?i)(fqdn|instance|host)', 'instance', pattern)
    if 'panels' in p:
        for pp in p['panels']:
            fixpanel(pp)

def fix(obj):
    dashboard = obj['dashboard']
    dashboard['__inputs'] = json.loads("""
    [
    {
      "name": "DS_PROMETHEUS",
      "label": "Prometheus",
      "description": "prometheus with data from postgres_exporter",
      "type": "datasource",
      "pluginId": "prometheus",
      "pluginName": "Prometheus"
    }
    ]
    """)
    dashboard['__requires'] = json.loads("""
    [
    {
      "type": "datasource",
      "id": "prometheus",
      "name": "Prometheus",
      "version": "1.0.0"
    }
    ]
    """)
    dashboard['annotations'] = json.loads("""{"list": []}""")
    dashboard['links'] = list()
    for p in dashboard['templating']['list']:
        if 'datasource' in  p:
            if p['datasource'] != 'Prometheus in Azure':
                print p['datasource']
            p['datasource'] = '${DS_PROMETHEUS}'
        if 'label' in p and 'name' in p:
            if p['label'].lower() in ('instance','host', 'fqdn'):
                p['label'] = 'Instance'
                p['name'] = 'instance'
                p['query'] = 'label_values(pg_up, instance)'
                p['datasource'] = '${DS_PROMETHEUS}'
    for p in dashboard['panels']:
        fixpanel(p)

    return dashboard

for fname in sys.argv[1:]:
    with open(fname, "r") as f:
        data = json.load(f)
        result = fix(data)
        with open("fixed_"+fname, "w") as f2:
            json.dump(result, f2, sort_keys=True, indent=2, separators=(',', ': '))



